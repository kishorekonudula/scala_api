package moverAPI

import moverAPI.utils.{DateUtils, IncrementUtils, SparkUtils}
import org.apache.spark.sql._
import org.apache.spark.{SparkConf, SparkContext}


object PmixInitialLoad_lvl2partition
  extends App {
  val appName = "PMIX initial load"
  val sparkConf = new SparkConf().setAppName(appName)
  val sc = new SparkContext(sparkConf)
  val sqlContext = new SQLContext(sc)


  // Convert today to SAS date
  val sas_today = DateUtils.sas_today()

  // Create the initial history pmix file
  var f_pos_pmix_hist_stag: DataFrame = sqlContext.emptyDataFrame
  for (i <- 21000 to (sas_today).toInt) {
    if (f_pos_pmix_hist_stag.rdd.isEmpty()) {
      f_pos_pmix_hist_stag = sqlContext.read.jdbc(SparkUtils.url, IncrementUtils.table, Array("TIME_KEY = " + i), SparkUtils.prop)
    } else {
      f_pos_pmix_hist_stag = f_pos_pmix_hist_stag.unionAll(sqlContext.read.jdbc(SparkUtils.url, IncrementUtils.table, Array("TIME_KEY = " + i), SparkUtils.prop))
    }
  }
  val d_mitm = sqlContext.read.parquet("/user/kkonudul/dwpd/d_mitm")
  val f_pos_pmix_hist = f_pos_pmix_hist_stag.withColumn("YEAR", DateUtils.convertSasDate(f_pos_pmix_hist_stag.col("TIME_KEY")))
    .withColumn("QTR", DateUtils.convertSasDateQtr(f_pos_pmix_hist_stag.col("TIME_KEY")))
    .join(d_mitm.select("MITM_KEY", "MITM_GRP_LEV2"), Seq("MITM_KEY"), "left")
  f_pos_pmix_hist.write.partitionBy("YEAR", "QTR", "MITM_GRP_LEV2").mode(SaveMode.Overwrite).parquet("/user/kkonudul/incremental_test/lvl2partition")



}



