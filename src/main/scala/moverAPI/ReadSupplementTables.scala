package moverAPI

import org.apache.spark.sql.SQLContext
import org.apache.spark.{SparkConf, SparkContext}

object ReadSupplementTables
  extends App {
  val appName = "Read PMIX from DWPD"
  val sparkConf = new SparkConf().setAppName(appName)
  val sc = new SparkContext(sparkConf)
  val sqlContext = new SQLContext(sc)
  //sqlContext.setConf("spark.sql.parquet.compression.codec", "snappy")

  val driver = "oracle.jdbc.driver.OracleDriver"
  Class.forName(driver).newInstance

  val url = "jdbc:oracle:thin:@dwpd-v2.databases.perseco.com:1521/DWPDOEL"
  val username = "mczachor"
  val password = "mczachor"
  val prop = new java.util.Properties
  prop.setProperty("user", username)
  prop.setProperty("password", password)
  prop.setProperty("driver", driver)
  prop.setProperty("fetchsize", "50000")
  prop.setProperty("numpartitions", "500")

  /*
  prop.setProperty("lowerBound", "0L")
  prop.setProperty("upperBound", "1000L")
  prop.setProperty("partitionColumn", "TIME_KEY")*/

  //val read_t1 = System.nanoTime

  val d_rest = sqlContext.read.jdbc(url, "d_rest", prop)
  d_rest.write.mode("overwrite").parquet("/user/kkonudul/dwpd/d_rest")

  val f_full_dly_trans = sqlContext.read.jdbc(url, "f_full_dly_trans",prop)
  f_full_dly_trans.write.mode("overwrite").parquet("/user/kkonudul/dwpd/f_full_dly_trans")

  val d_mitm = sqlContext.read.jdbc(url, "d_mitm", prop)
  d_mitm.write.mode("overwrite").parquet("/user/kkonudul/dwpd/d_mitm")
  //val duration = (System.nanoTime - read_t1) / 1e9d
  //println("PMIX read time: " + duration)
}



