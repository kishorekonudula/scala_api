package moverAPI

import moverAPI.utils.{DateUtils, IncrementUtils, SparkUtils}
import org.apache.spark.sql._
//import org.apache.spark.sql.types._
import org.apache.spark.{SparkConf, SparkContext}


object PmixInitialLoad_lvl4partition
  extends App {
  val appName = "PMIX initial load"
  val sparkConf = new SparkConf().setAppName(appName)
  val sc = new SparkContext(sparkConf)
  val sqlContext = new SQLContext(sc)


  // Convert today to SAS date
  val sas_today = DateUtils.sas_today()

  // Create the initial history pmix file
  var cnt = 0
  val d_mitm = sqlContext.read.parquet("/user/kkonudul/dwpd/d_mitm")
  var f_pos_pmix_hist_stag: DataFrame = sqlContext.emptyDataFrame
  //for (i <- 20645 to (sas_today).toInt) {
  // for (i <- 20279 to 20644) {
  //for (i <- 19914 to 20278) {
  //for (i <- 19184 to 19913) {
  for (i <- 19724 to sas_today.toInt) {
    if (f_pos_pmix_hist_stag.rdd.isEmpty()) {
      f_pos_pmix_hist_stag = sqlContext.read.jdbc(SparkUtils.url, IncrementUtils.table, Array("TIME_KEY = " + i), SparkUtils.prop)
    } else {
      f_pos_pmix_hist_stag = f_pos_pmix_hist_stag.unionAll(sqlContext.read.jdbc(SparkUtils.url, IncrementUtils.table, Array("TIME_KEY = " + i), SparkUtils.prop))
    }
    cnt += 1
    if (cnt == 182){
      val f_pos_pmix_hist = f_pos_pmix_hist_stag.withColumn("YEAR", DateUtils.convertSasDate(f_pos_pmix_hist_stag.col("TIME_KEY")))
        .join(d_mitm.select("MITM_KEY", "MITM_GRP_LEV4"), Seq("MITM_KEY"), "left")
      f_pos_pmix_hist.write.partitionBy("YEAR", "MITM_GRP_LEV4").mode(SaveMode.Append).parquet("/user/kkonudul/incremental_test/lvl4partition_new")
      f_pos_pmix_hist.unpersist(blocking = true)
      f_pos_pmix_hist_stag.unpersist(blocking = true)
      f_pos_pmix_hist_stag = sqlContext.emptyDataFrame
      cnt = 0
    }
  }

  val f_pos_pmix_hist = f_pos_pmix_hist_stag.withColumn("YEAR", DateUtils.convertSasDate(f_pos_pmix_hist_stag.col("TIME_KEY")))
    .join(d_mitm.select("MITM_KEY", "MITM_GRP_LEV4"), Seq("MITM_KEY"), "left")
  f_pos_pmix_hist.write.partitionBy("YEAR", "MITM_GRP_LEV4").mode(SaveMode.Append).parquet("/user/kkonudul/incremental_test/lvl4partition_new")
  f_pos_pmix_hist.unpersist(blocking = true)
/*
  //val d_mitm = sqlContext.read.parquet("/user/kkonudul/dwpd/d_mitm")
  //val d_mitm = sqlContext.read.parquet("C://Users//kkonudul//Downloads//d_mitm")
  val f_pos_pmix_hist = f_pos_pmix_hist_stag.withColumn("YEAR", DateUtils.convertSasDate(f_pos_pmix_hist_stag.col("TIME_KEY")))
    //.withColumn("QTR", DateUtils.convertSasDateQtr(f_pos_pmix_hist_stag.col("TIME_KEY")))
    .join(d_mitm.select("MITM_KEY", "MITM_GRP_LEV4"), Seq("MITM_KEY"), "left")
  //f_pos_pmix_hist.write.partitionBy("YEAR", "MITM_GRP_LEV4").mode(SaveMode.Append).parquet("/user/kkonudul/incremental_test/lvl4partition")
  f_pos_pmix_hist.show()
*/

}



