package moverAPI

import org.apache.spark.sql._
import org.apache.spark.{SparkConf, SparkContext}


object WritePostgres
  extends App {
  val appName = "Read PMIX from HDFS and write back to HDFS"
  val sparkConf = new SparkConf().setAppName(appName)
  //.setMaster("local[8]")
  val sc = new SparkContext(sparkConf)
  val sqlContext = new SQLContext(sc)
  //sqlContext.setConf("spark.sql.parquet.compression.codec", "snappy")

  val driver = "org.postgresql.Driver"
  Class.forName(driver).newInstance

  val url = "jdbc:postgresql://hgsdpfdna01:5432/analytics"
  val username = "analytics"
  val password = "analytics"
  val table = "pmix_hdfs"
  val prop = new java.util.Properties
  prop.setProperty("user", username)
  prop.setProperty("password", password)
  prop.setProperty("driver", driver)
  prop.setProperty("fetchsize", "50000")
  prop.setProperty("numpartitions", "500")

  /*
  prop.setProperty("lowerBound", "0L")
  prop.setProperty("upperBound", "1000L")
  prop.setProperty("partitionColumn", "TIME_KEY")
  */

  //val read_t1 = System.nanoTime

  //val duration = (System.nanoTime - read_t1) / 1e9d
  //println("PMIX read time: " + duration)


  val f_pos_pmix = sqlContext.read.parquet("/user/kkonudul/dwpd/f_pos_pmix_daytoday_read").where("MITM_KEY = 5")
  f_pos_pmix.write.jdbc(url, table, prop)

  //f_pos_pmix.write.parquet("/user/kkonudul/dwpd/pmix_write_test")

}



