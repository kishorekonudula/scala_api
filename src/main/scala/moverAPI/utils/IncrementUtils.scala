package moverAPI.utils

object IncrementUtils {
  val max_hist_days = 30
  //val start_date = 18634
  val start_date = 20820
  //val start_date = 20983

  val table = "f_pos_pmix"

  val folderPath = "/user/kkonudul/dwpd/f_pos_pmix_incremental/"
  //val folderPath = "C://Users//kkonudul//Downloads//pmix_test//"
  val hist = "f_pos_pmix_hist"
  val audit = "audit"
  val inc = "f_pos_pmix_inc"
}



