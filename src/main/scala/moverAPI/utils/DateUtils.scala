package moverAPI.utils

import java.text.SimpleDateFormat
import java.util.{Calendar, Date, GregorianCalendar}

import org.apache.spark.sql.functions


object DateUtils {
  def convertSasDate = functions.udf((t: Int) => {
    val ft = new SimpleDateFormat("yyyy-MM-dd")
    val epoch = ft.parse("1960-01-01")
    var c = Calendar.getInstance()
    c.setTime(epoch)
    c.add(Calendar.DATE, t)
    //new java.sql.Date(c.getTime.getTime())
    c.get(Calendar.YEAR)
  })

  def convertSasDateQtr = functions.udf((t: Int) => {
    val ft = new SimpleDateFormat("yyyy-MM-dd")
    val epoch = ft.parse("1960-01-01")
    var c = Calendar.getInstance()
    c.setTime(epoch)
    c.add(Calendar.DATE, t)
    //new java.sql.Date(c.getTime.getTime())
    (c.get(Calendar.MONTH) / 3) + 1
  })

  var today: Date = null
  def sas_today(): Long = {
    val ft = new SimpleDateFormat("yyyy-MM-dd")
    val epoch = ft.parse("1960-01-01")
    val t: GregorianCalendar = new GregorianCalendar() //today
    t.set(Calendar.HOUR_OF_DAY, 0)
    t.set(Calendar.MINUTE, 0)
    t.set(Calendar.SECOND, 0)
    t.set(Calendar.MILLISECOND, 0)
    today = t.getTime()

    //val today = ft.parse("2017-06-15")
    math.round((today.getTime - epoch.getTime) / (1000.0 * 60 * 60 * 24))
  }

}



