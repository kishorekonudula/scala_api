package moverAPI

import moverAPI.utils.{DateUtils, IncrementUtils, SparkUtils}
import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.sql.{DataFrame, SQLContext, SaveMode}

object test
  extends App {
  val appName = "PMIX load"
  val sparkConf = new SparkConf().setAppName(appName).setMaster("local[8]")
  val sc = new SparkContext(sparkConf)
  sc.setLogLevel("ERROR")
  sc.hadoopConfiguration.set("parquet.enable.summary-metadata", "false")
  val sqlContext = new SQLContext(sc)

  // Convert today to SAS date
  val sas_today = DateUtils.sas_today()

  /*val hist = sqlContext.read.parquet("C://Users//kkonudul//Downloads//pmix_test//f_pos_pmix_hist")
  val audit = sqlContext.read.parquet("C://Users//kkonudul//Downloads//pmix_test//audit")
  val inc = sqlContext.read.parquet("C://Users//kkonudul//Downloads//pmix_test//f_pos_pmix_inc")

  print("Hist file")
  hist.groupBy().max("TIME_KEY").show()
  hist.groupBy().min("TIME_KEY").show()

  print("audit")
  audit.show()

  print("Inc")
  inc.groupBy().max("TIME_KEY").show()
  inc.groupBy().min("TIME_KEY").show()*/


  val d_mitm = sqlContext.read.parquet("C://Users//kkonudul//Downloads//d_mitm")
  var f_pos_pmix_inc_stag: DataFrame = sqlContext.emptyDataFrame
  for (i <- (sas_today - IncrementUtils.max_hist_days).toInt + 1 to sas_today.toInt) {
    if (f_pos_pmix_inc_stag.rdd.isEmpty()) {
      f_pos_pmix_inc_stag = sqlContext.read.jdbc(SparkUtils.url, IncrementUtils.table, Array("TIME_KEY = " + i), SparkUtils.prop)
    } else {
      f_pos_pmix_inc_stag = f_pos_pmix_inc_stag.unionAll(sqlContext.read.jdbc(SparkUtils.url, IncrementUtils.table, Array("TIME_KEY = " + i), SparkUtils.prop))
    }
  }
  val f_pos_pmix_inc = f_pos_pmix_inc_stag.withColumn("YEAR", DateUtils.convertSasDate(f_pos_pmix_inc_stag.col("TIME_KEY")))
    .join(d_mitm.select("MITM_KEY", "MITM_GRP_LEV4"), Seq("MITM_KEY"), "left")
  f_pos_pmix_inc.groupBy().max("TIME_KEY").show()
  f_pos_pmix_inc.groupBy().min("TIME_KEY").show()


}



