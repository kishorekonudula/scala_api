package moverAPI

import org.apache.spark.sql.{DataFrame, SQLContext}
import org.apache.spark.{SparkConf, SparkContext}

object ReadPmixDaybyDay
  extends App {
  val appName = "Read PMIX from DWPD"
  val sparkConf = new SparkConf().setAppName(appName)
  //.setMaster("local[8]")
  val sc = new SparkContext(sparkConf)
  val sqlContext = new SQLContext(sc)
  //sqlContext.setConf("spark.sql.parquet.compression.codec", "snappy")

  val driver = "oracle.jdbc.driver.OracleDriver"
  Class.forName(driver).newInstance

  val url = "jdbc:oracle:thin:@dwpd-v2.databases.perseco.com:1521/DWPDOEL"
  val username = "mczachor"
  val password = "mczachor"
  val table = "f_pos_pmix"
  val prop = new java.util.Properties
  prop.setProperty("user", username)
  prop.setProperty("password", password)
  prop.setProperty("driver", driver)
  prop.setProperty("fetchsize", "50000")
  prop.setProperty("numpartitions", "500")

  prop.setProperty("lowerBound", "0L")
  prop.setProperty("upperBound", "1000L")
  prop.setProperty("partitionColumn", "TIME_KEY")

  //val read_t1 = System.nanoTime

  var f_pos_pmix: DataFrame = sqlContext.emptyDataFrame
  for (i <- 20200 to 21005) {
    if (f_pos_pmix.rdd.isEmpty()) {
      f_pos_pmix = sqlContext.read.jdbc(url, table, Array("TIME_KEY = " + i), prop)
    } else {
      f_pos_pmix = f_pos_pmix.unionAll(sqlContext.read.jdbc(url, table, Array("TIME_KEY = " + i), prop))
    }
  }
  f_pos_pmix.write.mode("overwrite").parquet("/user/kkonudul/dwpd/f_pos_pmix_daytoday_read")

  //val duration = (System.nanoTime - read_t1) / 1e9d
  //println("PMIX read time: " + duration)
}



