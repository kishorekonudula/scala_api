package moverAPI

import org.apache.spark.rdd.RDD
import org.apache.spark.sql._
import org.apache.spark.sql.types._
import org.apache.spark.{SparkConf, SparkContext}
import moverAPI.utils.{DateUtils, IncrementUtils, SparkUtils}


object PmixInitialLoad
  extends App {
  val appName = "PMIX initial load"
  val sparkConf = new SparkConf().setAppName(appName)
  val sc = new SparkContext(sparkConf)
  val sqlContext = new SQLContext(sc)


  // Convert today to SAS date
  val sas_today = DateUtils.sas_today()

  // Create the initial history pmix file
  var cnt = 0
  val d_mitm = sqlContext.read.parquet("/user/kkonudul/dwpd/d_mitm")
  //val d_mitm = sqlContext.read.parquet("C://Users//kkonudul//Downloads//d_mitm")
  var f_pos_pmix_hist_stag: DataFrame = sqlContext.emptyDataFrame
  for (i <- IncrementUtils.start_date to (sas_today - IncrementUtils.max_hist_days).toInt) {
    if (f_pos_pmix_hist_stag.rdd.isEmpty()) {
      f_pos_pmix_hist_stag = sqlContext.read.jdbc(SparkUtils.url, IncrementUtils.table, Array("TIME_KEY = " + i), SparkUtils.prop)
    } else {
      f_pos_pmix_hist_stag = f_pos_pmix_hist_stag.unionAll(sqlContext.read.jdbc(SparkUtils.url, IncrementUtils.table, Array("TIME_KEY = " + i), SparkUtils.prop))
    }
    cnt += 1
    if (cnt == 182){
      var f_pos_pmix_hist = f_pos_pmix_hist_stag.withColumn("YEAR", DateUtils.convertSasDate(f_pos_pmix_hist_stag.col("TIME_KEY")))
        .join(d_mitm.select("MITM_KEY", "MITM_GRP_LEV4"), Seq("MITM_KEY"), "left")
      f_pos_pmix_hist.write.partitionBy("YEAR", "MITM_GRP_LEV4").mode(SaveMode.Append).parquet(IncrementUtils.folderPath + IncrementUtils.hist)
      f_pos_pmix_hist.unpersist(blocking = true)
      f_pos_pmix_hist = sqlContext.emptyDataFrame
      f_pos_pmix_hist_stag.unpersist(blocking = true)
      f_pos_pmix_hist_stag = sqlContext.emptyDataFrame
      cnt = 0
    }
  }
  var f_pos_pmix_hist = f_pos_pmix_hist_stag.withColumn("YEAR", DateUtils.convertSasDate(f_pos_pmix_hist_stag.col("TIME_KEY")))
    .join(d_mitm.select("MITM_KEY", "MITM_GRP_LEV4"), Seq("MITM_KEY"), "left")
  f_pos_pmix_hist.write.partitionBy("YEAR", "MITM_GRP_LEV4").mode(SaveMode.Append).parquet(IncrementUtils.folderPath + IncrementUtils.hist)
  f_pos_pmix_hist.unpersist(blocking = true)
  f_pos_pmix_hist = sqlContext.emptyDataFrame

  // Write the last date in the history file to the audit file
  val hist_last: Array[Row] = Array(Row(sas_today - IncrementUtils.max_hist_days, new java.sql.Date(DateUtils.today.getTime)))
  val rd: RDD[Row] = sc.parallelize(hist_last)
  val schema = StructType(Array(StructField("TIME_KEY", LongType), StructField("DATE", DateType)))
  val df = sqlContext.createDataFrame(rd, schema)
  df.write.mode(SaveMode.Overwrite).parquet(IncrementUtils.folderPath + IncrementUtils.audit)


  // Create the initial incremental file with data from IncrementUtils.max_hist_days
  var f_pos_pmix_inc_stag: DataFrame = sqlContext.emptyDataFrame
  for (i <- (sas_today - IncrementUtils.max_hist_days).toInt + 1 to sas_today.toInt) {
    if (f_pos_pmix_inc_stag.rdd.isEmpty()) {
      f_pos_pmix_inc_stag = sqlContext.read.jdbc(SparkUtils.url, IncrementUtils.table, Array("TIME_KEY = " + i), SparkUtils.prop)
    } else {
      f_pos_pmix_inc_stag = f_pos_pmix_inc_stag.unionAll(sqlContext.read.jdbc(SparkUtils.url, IncrementUtils.table, Array("TIME_KEY = " + i), SparkUtils.prop))
    }
  }
  var f_pos_pmix_inc = f_pos_pmix_inc_stag.withColumn("YEAR", DateUtils.convertSasDate(f_pos_pmix_inc_stag.col("TIME_KEY")))
    .join(d_mitm.select("MITM_KEY", "MITM_GRP_LEV4"), Seq("MITM_KEY"), "left")
  f_pos_pmix_inc.write.partitionBy("YEAR", "MITM_GRP_LEV4").mode(SaveMode.Overwrite).parquet(IncrementUtils.folderPath + IncrementUtils.inc)
  f_pos_pmix_inc.unpersist(blocking = false)
  f_pos_pmix_inc = sqlContext.emptyDataFrame
  sc.stop()
}