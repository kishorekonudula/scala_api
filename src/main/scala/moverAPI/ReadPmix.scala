package moverAPI

import moverAPI.utils.{DateUtils, IncrementUtils, SparkUtils}
import org.apache.spark.sql.{DataFrame, SQLContext, SaveMode}
import org.apache.spark.{SparkConf, SparkContext}

object ReadPmix
  extends App {
  val appName = "Read PMIX from DWPD"
  val sparkConf = new SparkConf().setAppName(appName)
  //.setMaster("local[8]")
  val sc = new SparkContext(sparkConf)
  val sqlContext = new SQLContext(sc)
  //sqlContext.setConf("spark.sql.parquet.compression.codec", "snappy")

  val driver = "oracle.jdbc.driver.OracleDriver"
  Class.forName(driver).newInstance

  val url = "jdbc:oracle:thin:@dwpd-v2.databases.perseco.com:1521/DWPDOEL"
  val username = "mczachor"
  val password = "mczachor"
  val table = "f_pos_pmix"
  val prop = new java.util.Properties
  prop.setProperty("user", username)
  prop.setProperty("password", password)
  prop.setProperty("driver", driver)
  prop.setProperty("fetchsize", "50000")
  prop.setProperty("numpartitions", "500")

  prop.setProperty("lowerBound", "0L")
  prop.setProperty("upperBound", "1000L")
  prop.setProperty("partitionColumn", "TIME_KEY")

  var f_pos_pmix_hist_stag: DataFrame = sqlContext.emptyDataFrame
  //for (i <- 18634 to 19365) {
  for (i <- 19366 to 20199) {
    if (f_pos_pmix_hist_stag.rdd.isEmpty()) {
      f_pos_pmix_hist_stag = sqlContext.read.jdbc(SparkUtils.url, IncrementUtils.table, Array("TIME_KEY = " + i), SparkUtils.prop)
    } else {
      f_pos_pmix_hist_stag = f_pos_pmix_hist_stag.unionAll(sqlContext.read.jdbc(SparkUtils.url, IncrementUtils.table, Array("TIME_KEY = " + i), SparkUtils.prop))
    }
  }
  val f_pos_pmix_hist = f_pos_pmix_hist_stag.withColumn("YEAR", DateUtils.convertSasDate(f_pos_pmix_hist_stag.col("TIME_KEY")))
    .withColumn("QTR", DateUtils.convertSasDateQtr(f_pos_pmix_hist_stag.col("TIME_KEY")))
  f_pos_pmix_hist.write.partitionBy("YEAR", "QTR").mode(SaveMode.Append).parquet(IncrementUtils.folderPath + IncrementUtils.hist)



}



