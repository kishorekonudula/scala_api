package moverAPI

import moverAPI.utils.{DateUtils, IncrementUtils, SparkUtils}
import org.apache.spark.sql.{DataFrame, Row, SQLContext, SaveMode}
import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.ml.feature.QuantileDiscretizer
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.types.DoubleType

object partitionExample
  extends App {
  val appName = "Partition example"
  val sparkConf = new SparkConf().setAppName(appName).setMaster("local[8]")
  val sc = new SparkContext(sparkConf)
  val sqlContext = new SQLContext(sc)

  val table = "f_pos_pmix"

  val read_t1 = System.nanoTime

  var f_pos_pmix_hist_stag: DataFrame = sqlContext.emptyDataFrame
  for (i <- 21012 to 21012) {
    if (f_pos_pmix_hist_stag.rdd.isEmpty()) {
      f_pos_pmix_hist_stag = sqlContext.read.jdbc(SparkUtils.url, IncrementUtils.table, Array("TIME_KEY = " + i), SparkUtils.prop)
    } else {
      f_pos_pmix_hist_stag = f_pos_pmix_hist_stag.unionAll(sqlContext.read.jdbc(SparkUtils.url, IncrementUtils.table, Array("TIME_KEY = " + i), SparkUtils.prop))
    }
  }
  val f_pos_pmix_hist = f_pos_pmix_hist_stag.withColumn("YEAR", DateUtils.convertSasDate(f_pos_pmix_hist_stag.col("TIME_KEY")))
    .withColumn("QTR", DateUtils.convertSasDateQtr(f_pos_pmix_hist_stag.col("TIME_KEY")))

  //f_pos_pmix_hist.write.mode(SaveMode.Overwrite).parquet("/user/kkonudul/incremental_test/no_partition")
  //f_pos_pmix_hist.write.partitionBy("YEAR", "MITM_KEY").mode(SaveMode.Overwrite).parquet("/user/kkonudul/incremental_test/mitm_partition")

  //f_pos_pmix_hist.select("MITM_KEY").dropDuplicates(Array("MITM_KEY")).show()
  val a = f_pos_pmix_hist.withColumn("MITM_KEY", f_pos_pmix_hist.col("MITM_KEY").cast(DoubleType))
  val discretizer = new QuantileDiscretizer().setInputCol("MITM_KEY").setOutputCol("MITM_BUCKET").setNumBuckets(4)
  val result = discretizer.fit(a).transform(a)
  result.groupBy("MITM_BUCKET").count().show()

  val x = a.select("MITM_KEY").map(value => value.getDouble(0)).histogram(10)
  for(i <- x._1){
    println(i)
  }
  println()
  for(i <- x._2){
    println(i)
  }

  val duration = (System.nanoTime - read_t1) / 1e9d
  println("PMIX read time: " + duration)
}



