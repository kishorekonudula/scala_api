package moverAPI


import moverAPI.utils.{DateUtils, IncrementUtils, SparkUtils}
import org.apache.spark.rdd.RDD
import org.apache.spark.sql._
import org.apache.spark.sql.types._
import org.apache.spark.{SparkConf, SparkContext}


object PmixIncremental
  extends App {
  val appName = "PMIX incremental load"
  val sparkConf = new SparkConf().setAppName(appName)
  val sc = new SparkContext(sparkConf)
  val sqlContext = new SQLContext(sc)

  // Convert today to SAS date
  val sas_today = DateUtils.sas_today()

  val d_mitm = sqlContext.read.parquet("/user/kkonudul/dwpd/d_mitm")

  // Read the audit file to get the last loaded date in hist file
  val audit = sqlContext.read.parquet(IncrementUtils.folderPath + IncrementUtils.audit)
  val last_val = audit.agg(functions.max("TIME_KEY")).select("max(TIME_KEY)").collectAsList().get(0).getLong(0)

  // Append the day after last day as read from the audit file to the history file
  val hist_append_stage = sqlContext.read.jdbc(SparkUtils.url, IncrementUtils.table, Array("TIME_KEY = " + (last_val + 1)), SparkUtils.prop)
  val hist_append =  hist_append_stage.withColumn("YEAR", DateUtils.convertSasDate(hist_append_stage.col("TIME_KEY")))
    .join(d_mitm.select("MITM_KEY", "MITM_GRP_LEV4"), Seq("MITM_KEY"), "left")
  hist_append.write.partitionBy("YEAR", "MITM_GRP_LEV4").mode(SaveMode.Append).parquet(IncrementUtils.folderPath + IncrementUtils.hist)

  // Read the last 30 days of data and overwrite the incremental file
  var inc_stage: DataFrame = sqlContext.emptyDataFrame
  for (i <- (last_val + 2).toInt to sas_today.toInt) {
    if (inc_stage.rdd.isEmpty()) {
      inc_stage = sqlContext.read.jdbc(SparkUtils.url, IncrementUtils.table, Array("TIME_KEY = " + i), SparkUtils.prop)
    } else {
      inc_stage = inc_stage.unionAll(sqlContext.read.jdbc(SparkUtils.url, IncrementUtils.table, Array("TIME_KEY = " + i), SparkUtils.prop))
    }
  }
  val inc = inc_stage.withColumn("YEAR", DateUtils.convertSasDate(inc_stage.col("TIME_KEY")))
    .join(d_mitm.select("MITM_KEY", "MITM_GRP_LEV4"), Seq("MITM_KEY"), "left")
  inc.write.partitionBy("YEAR", "MITM_GRP_LEV4").mode(SaveMode.Overwrite).parquet(IncrementUtils.folderPath + IncrementUtils.inc)

  // Append a record in the audit file with the last date that was written to the history file
  val hist_last: Array[Row] = Array(Row((last_val + 1), new java.sql.Date(DateUtils.today.getTime)))
  val rd: RDD[Row] = sc.parallelize(hist_last)
  val schema = StructType(Array(StructField("TIME_KEY", LongType), StructField("DATE", DateType)))
  val df = sqlContext.createDataFrame(rd, schema)
  df.write.mode(SaveMode.Append).parquet(IncrementUtils.folderPath + IncrementUtils.audit)
}



