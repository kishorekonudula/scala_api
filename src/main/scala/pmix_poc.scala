import org.apache.spark.sql.types._
import org.apache.spark.sql.{Column, DataFrame, SQLContext, functions}
import org.apache.spark.{SparkConf, SparkContext}

object pmix_poc
  extends App {
  val appName = "pmix_poc"
  val sparkConf = new SparkConf().setAppName(appName).setMaster("local[8]")
  val sc = new SparkContext(sparkConf)
  val sqlContext = new SQLContext(sc)
  //sqlContext.setConf("spark.sql.parquet.compression.codec", "snappy")

  val driver = "oracle.jdbc.driver.OracleDriver"
  Class.forName(driver).newInstance

  val url = "jdbc:oracle:thin:@dwpd-v2.databases.perseco.com:1521/DWPDOEL"
  val username = "mczachor"
  val password = "mczachor"
  val table = "f_pos_pmix"
  val prop = new java.util.Properties
  prop.setProperty("user", username)
  prop.setProperty("password", password)
  prop.setProperty("driver", driver)
  prop.setProperty("fetchsize", "50000")
  prop.setProperty("numpartitions", "100")

  prop.setProperty("lowerBound", "0L")
  prop.setProperty("upperBound", "100L")
  prop.setProperty("partitionColumn", "TIME_KEY")

  val read_t1 = System.nanoTime
/*
  //val f_pos_pmix = sqlContext.read.jdbc(url, table, Array("TIME_KEY = 20927", "MITM_KEY = 1"), prop).where("MITM_KEY = 1")
  val f_pos_pmix = sqlContext.read.jdbc(url, table, Array("TIME_KEY >= 20200 and MITM_KEY = 5"), prop)//.where("MITM_KEY = 1")
  //val f_pos_pmix = sqlContext.read.jdbc(url, "d_rest", prop)
  //val a = f_pos_pmix.agg(functions.avg("UNITS_SOLD"))
  //f_pos_pmix.show()


  val write_t1 = System.nanoTime
  f_pos_pmix.write.parquet("/user/kkonudul/api_hadoop_comparison/samplecsv")
  val write_duration = (System.nanoTime - write_t1) / 1e9d
  print("PMIX write time: " + write_duration)*/

  /*
  // Pulling from /user/kkonudul/nexus/rest_key_metrics/rest_agg.csv to measure the time taken
  val read_t1 = System.nanoTime
  val rest_schema = StructType(Array(StructField("geog_key", IntegerType), StructField("time_key", IntegerType), StructField("mitm_key", IntegerType), StructField("total_units", IntegerType), StructField("adus", DoubleType), StructField("rest_key_count", IntegerType), StructField("total_dollars", DoubleType), StructField("combo_units", IntegerType), StructField("rev0", DoubleType), StructField("trans", DoubleType), StructField("units_sold", IntegerType), StructField("promo_units", IntegerType), StructField("free_pct", DoubleType), StructField("combo_pct", DoubleType), StructField("upt", DoubleType), StructField("total_units_ixn", DoubleType), StructField("adus_ixn", DoubleType), StructField("rest_key_count_ixn", DoubleType), StructField("total_dollars_ixn", DoubleType), StructField("combo_units_ixn", DoubleType), StructField("rev0_ixn", DoubleType), StructField("trans_ixn", DoubleType), StructField("units_sold_ixn", DoubleType), StructField("promo_units_ixn", DoubleType), StructField("free_pct_ixn", DoubleType), StructField("combo_pct_ixn", DoubleType), StructField("upt_ixn", DoubleType), StructField("time_key_txt", StringType), StructField("geog_level", StringType)))
  val rest_agg = sqlContext.read.format("com.databricks.spark.csv").option("header", "true").schema(rest_schema).load("/user/kkonudul/nexus/rest_key_metrics/rest_agg.csv")
      .where("mitm_key = 5")
  val row_count = rest_agg.count()
  print(row_count)
  val read_duration = (System.nanoTime - read_t1) / 1e9d
  println("PMIX read time: " + read_duration)
  */

  /*
    var f_pos_pmix = sqlContext.read.parquet("/user/kkonudul/dwpd/f_pos_pmix_incremental/f_pos_pmix_hist").where("MITM_KEY = 5").
      unionAll(sqlContext.read.parquet("/user/kkonudul/dwpd/f_pos_pmix_incremental/f_pos_pmix_inc").where("MITM_KEY = 5"))
    //val f_pos_pmix = sqlContext.read.parquet("/user/kkonudul/dwpd/f_pos_pmix_incremental/f_pos_pmix_hist").where("MITM_KEY = 5 and REST_KEY = 10746")
    //  .unionAll(sqlContext.read.parquet("/user/kkonudul/dwpd/f_pos_pmix_incremental/f_pos_pmix_inc").where("MITM_KEY = 5 and REST_KEY = 10746") )
    val f_pos_pmix = sqlContext.read.parquet("/user/kkonudul/dwpd/f_pos_pmix_incremental/f_pos_pmix_hist").where("TIME_KEY >= 20610 and MITM_KEY = 5 and REST_KEY = 10746")
      .unionAll(sqlContext.read.parquet("/user/kkonudul/dwpd/f_pos_pmix_incremental/f_pos_pmix_inc").where("TIME_KEY >= 20610 and MITM_KEY = 5 and REST_KEY = 10746"))

    // Read parquet file and count records
    val row_count = f_pos_pmix.count()
    println(row_count)
  */
  /*var f_pos_pmix = sqlContext.read.parquet("/user/kkonudul/dwpd/f_pos_pmix_incremental/f_pos_pmix_hist").
    unionAll(sqlContext.read.parquet("/user/kkonudul/dwpd/f_pos_pmix_incremental/f_pos_pmix_inc"))

    // Get average menu item price
    val avg_mitm_price = f_pos_pmix.groupBy("TIME_KEY", "REST_KEY").agg(functions.avg("MITM_PRICE"))
    avg_mitm_price.show()
    print(avg_mitm_price.count())*/


  /*
    // Get count of distinct MITM_KEY
    //val f_pos_pmix = sqlContext.read.parquet("/user/kkonudul/dwpd/f_pos_pmix").where("TIME_KEY >= 20200")
    val mitms = f_pos_pmix.dropDuplicates(Array("MITM_KEY"))
    print(mitms.count())
  */

  /*
  // Get count of records
  val f_pos_pmix = sqlContext.read.parquet("/user/kkonudul/dwpd/f_pos_pmix")
  print(f_pos_pmix.count())
  */

  /*
    // Read file and write using snappy compression
    val f_pos_pmix = sqlContext.read.parquet("/user/kkonudul/dwpd/f_pos_pmix_daytoday_read")
    f_pos_pmix.write.mode("overwrite").parquet("/user/kkonudul/dwpd/f_pos_pmix_snappy")
  */

  /*
  // Read Snappy file and calculate average price per mitm
  val f_pos_pmix = sqlContext.read.parquet("/user/kkonudul/dwpd/f_pos_pmix_snappy").where("TIME_KEY >= 20200 and MITM_KEY = 5")
  val avg_mitm_price = f_pos_pmix.groupBy("TIME_KEY", "REST_KEY").agg(functions.avg("MITM_PRICE"))
  avg_mitm_price.show()
  print(avg_mitm_price.count())
  */

  /*
  // Read snappy file and get count of distinct MITM_KEY
  val f_pos_pmix = sqlContext.read.parquet("/user/kkonudul/dwpd/f_pos_pmix_snappy").where("TIME_KEY >= 20200")
  val mitms = f_pos_pmix.dropDuplicates(Array("MITM_KEY"))
  print(mitms.count())
  */

  /*
  // Read snappy file and get count of rows
  val f_pos_pmix = sqlContext.read.parquet("/user/kkonudul/dwpd/f_pos_pmix_snappy").where("TIME_KEY >= 20200")
  print(f_pos_pmix.count())
  */

/*
    // Read in pmix data after 20200 and write to Hadoop in Parquet
    var f_pos_pmix = sqlContext.read.parquet("/user/kkonudul/dwpd/f_pos_pmix_incremental/old/f_pos_pmix_hist", "/user/kkonudul/dwpd/f_pos_pmix_incremental/old/f_pos_pmix_inc")
      //unionAll(sqlContext.read.parquet("/user/kkonudul/dwpd/f_pos_pmix_incremental/old/f_pos_pmix_inc").where("TIME_KEY>= 20608 and MITM_KEY = 5"))
    //  val f_pos_pmix = sqlContext.read.jdbc(url, table, Array("TIME_KEY >= 20815"), prop)
    //f_pos_pmix.repartition(100, f_pos_pmix.col("TIME_KEY")).write.parquet("C://projects//nexus POC//dly_trans")

    f_pos_pmix.where(f_pos_pmix.col("TIME_KEY").geq(20608) && f_pos_pmix.col("MITM_KEY").isin(5,1))
      .write.parquet("/user/kkonudul/dwpd/pmix_write_test1")
  //  println(f_pos_pmix.rdd.partitions.length)
*/
  /*
  var f_pos_pmix: DataFrame = sqlContext.emptyDataFrame
  for (i <- 20608 to 20980) {
    if (f_pos_pmix.rdd.isEmpty()) {
      f_pos_pmix = sqlContext.read.jdbc(url, table, Array("TIME_KEY = " + i + " and MITM_KEY in (5,1)"), prop)
    } else {
      f_pos_pmix = f_pos_pmix.unionAll(sqlContext.read.jdbc(url, table, Array("TIME_KEY = " + i + " and MITM_KEY in (5,1)"), prop))
    }
  }
  f_pos_pmix.write.parquet("/user/kkonudul/dwpd/pmix_write_test")*/

  // Read in PMIX and partition on menu item key
  val f_pos_pmix = sqlContext.read.jdbc(url, table, Array("TIME_KEY >= 20939"), prop)
  f_pos_pmix.write.mode("overwrite").partitionBy("MITM_KEY").parquet("C:\\projects\\nexus POC\\incremental_test\\partition_example")

  val read_duration = (System.nanoTime - read_t1) / 1e9d
  println("PMIX read time: " + read_duration)
}



